HTML_PAGE_1 = "<html><head><title>Example page 1</title></head><body><p>Example paragraph</p><p><b>Second, " \
              "bold paragraph</b></p></body></html>"
SCRAPED_TEXT_HTML_PAGE_1 = "Example page 1 Example paragraph Second, bold paragraph"

HTML_PAGE_2 = '<html><head><title>Example page 2</title></head><body><img src="https://www.test1.com/images/logo.png" ' \
              'alt="Alt text" /><meta property="og:image" content="https://test2.com/images/92db9e.jpg"><link ' \
              'rel="icon" href="https://test3.com/images/devcbnf3.png"><p>Example <b>paragraph</b><img ' \
              'src="/images/relative.svg" alt="Alt text" /></p><div ' \
              'data-background="https://test4.pl//images/68125926-25597.jpg"></div><img ' \
              'data-src="https://test5.pl//images/it1.png?lm=15084" ' \
              'data-original="https://test5.pl//images/it1.png?lm=15084"</body></html> '
SCRAPED_IMG_1 = "https://www.test1.com/images/logo.png"
SCRAPED_IMG_2 = "https://test3.pl/images/relative.svg"
SCRAPED_IMG_3 = "https://test5.pl//images/it1.png?lm=15084"
SCRAPED_IMG_4 = "https://test2.com/images/92db9e.jpg"
SCRAPED_IMG_5 = "https://test3.com/images/devcbnf3.png"
SCRAPED_IMG_6 = "https://test4.pl//images/68125926-25597.jpg"

headers = {
    "user-agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0",
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "pl,en-US;q=0.7,en;q=0.3"
}
