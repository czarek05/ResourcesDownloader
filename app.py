import logging.config
import os
import urllib.error
import urllib.request as req
import uuid
from zipfile import ZipFile

import requests
from bs4 import BeautifulSoup
from flask import Flask, request, send_from_directory

import constants as cons
from TextHTMLParser import TextHTMLParser

logging.config.fileConfig('logging.conf')
logger = logging.getLogger('root')

app = Flask(__name__)


@app.route('/text/<path:variable>')
def scrape_text(variable):
    passed_url = request.full_path[6:]
    r, base_url = perform_external_request(passed_url)
    if type(r) is str:
        return r
    parser = TextHTMLParser()
    parser.feed(r.text)
    file_name = ""
    file_prefix = base_url[base_url.find('/') + 2:] + '_'
    try:
        with open("scraped_data/" + file_prefix + uuid.uuid4().hex[:6].upper() + ".txt", mode="w", encoding="utf-8") as txtFile:
            txtFile.write(" ".join(parser.found_text))
            file_name += txtFile.name[13:]
            logger.info("Writing to file: " + file_name + " was successful")
    except:
        logger.warning("Something gone wrong while writing to file: " + txtFile.name[13:])
        return "Something gone wrong while writing to file: " + txtFile.name[13:]
    return "Downloading text was successful. Saved file: " + file_name


@app.route('/img/<path:variable>')
def scrape_images(variable):
    passed_url = request.full_path[5:]
    r, base_url = perform_external_request(passed_url)
    if type(r) is str:
        return r
    soup = BeautifulSoup(r.text, 'html.parser')
    urls = []
    common_image_file_types = [".apng", ".bmp", ".gif", ".ico", ".cur",
                               ".jpg", ".jpeg", ".jfif", ".pjpeg", ".pjp",
                               ".png", ".svg", ".tif", ".tiff", ".webp"
                               ]
    img_tags = soup.find_all("img")
    img_urls = []
    for img_tag in img_tags:
        if "src" in img_tag.attrs:
            img_urls.append(img_tag.attrs["src"])
        if "data-src" in img_tag.attrs:
            img_urls.append(img_tag.attrs["data-src"])
        if "data-original" in img_tag.attrs:
            img_urls.append(img_tag.attrs["data-original"])

    for i in range(0, len(img_urls)):
        if not any([img_urls[i].startswith("http://"),
                    img_urls[i].startswith("https://"),
                    img_urls[i].startswith("//")]):
            if img_urls[i].startswith("/"):
                img_urls[i] = base_url + img_urls[i]
            else:
                img_urls[i] = base_url + '/' + img_urls[i]
    for url in img_urls:
        if url not in urls:
            urls.append(url)

    meta_tags = soup.find_all("meta")
    meta_urls = []
    for tag in meta_tags:
        if "content" in tag.attrs:
            content = tag.attrs["content"]
            if "." not in content:
                continue
            suffix = content[content.rfind('.'):content.rfind('.') + 6]
            for ext in common_image_file_types:
                if suffix.startswith(ext):
                    if not any([content.startswith("http://"),
                                content.startswith("https://"),
                                content.startswith("//")]):
                        if content.startswith('/'):
                            meta_urls.append(base_url + content)
                        else:
                            meta_urls.append(base_url + '/' + content)
                    else:
                        meta_urls.append(content)
                    break

    for url in meta_urls:
        if url not in urls:
            urls.append(url)

    link_tags = soup.find_all("link")
    link_urls = []
    for tag in link_tags:
        if "href" in tag.attrs:
            href = tag.attrs["href"]
            if "." not in href:
                continue
            suffix = href[href.rfind('.'):href.rfind('.') + 6]
            for ext in common_image_file_types:
                if suffix.startswith(ext):
                    if not any([href.startswith("http://"),
                                href.startswith("https://"),
                                href.startswith("//")]):
                        if href.startswith('/'):
                            link_urls.append(base_url + href)
                        else:
                            link_urls.append(base_url + '/' + href)
                    else:
                        link_urls.append(href)
                    break

    for url in link_urls:
        if url not in urls:
            urls.append(url)

    div_tags = soup.find_all("div")
    div_urls = []
    for tag in div_tags:
        if "data-background" in tag.attrs:
            data_background = tag.attrs["data-background"]
            if "." not in data_background:
                continue
            suffix = data_background[data_background.rfind('.'):data_background.rfind('.') + 6]
            for ext in common_image_file_types:
                if suffix.startswith(ext):
                    if not any([data_background.startswith("http://"),
                                data_background.startswith("https://"),
                                data_background.startswith("//")]):
                        if data_background.startswith('/'):
                            div_urls.append(base_url + data_background)
                        else:
                            div_urls.append(base_url + '/' + data_background)
                    else:
                        div_urls.append(data_background)
                    break

    for url in div_urls:
        if url not in urls:
            urls.append(url)

    logger.info("Found " + str(len(urls)) + " image urls")
    scrape_success_counter = 0
    scrape_failure_counter = 0
    file_prefix = base_url[base_url.find('/') + 2:] + '_'
    for url in urls:
        suffix = url[url.rfind('.'):url.rfind('.') + 6]
        localization = "scraped_data/" + file_prefix + uuid.uuid4().hex[:6].upper()
        for extension in common_image_file_types:
            if suffix.startswith(extension):
                localization += extension
                break
        else:
            localization += ".jpg"
        if url.startswith("//"):
            url = "https:" + url
        try:
            req.urlretrieve(url, localization)
            scrape_success_counter += 1
        except (urllib.error.HTTPError, urllib.error.URLError, urllib.error.ContentTooShortError):
            logger.info("Failed with following url: " + url)
            scrape_failure_counter += 1
    log = "Downloading images from " + passed_url + " completed. Successful downloads: " + str(scrape_success_counter)
    log += ". Failed downloads: " + str(scrape_failure_counter)
    logger.info(log)
    return log


@app.route('/download/')
def download():
    with ZipFile('scraped_data.zip', 'w') as zipObj:
        for dirpath, dirnames, filenames in os.walk("scraped_data"):
            for filename in filenames:
                filePath = os.path.join(dirpath, filename)
                zipObj.write(filePath)
    return send_from_directory(directory="",
                               filename='scraped_data.zip',
                               as_attachment=True,
                               cache_timeout=0.0)


def perform_external_request(passed_url):
    try:
        logger.info("Performing request to: " + passed_url)
        r = requests.get(passed_url, headers=cons.headers)
    except requests.exceptions.MissingSchema as err:
        logger.warning("Error with request occured: MissingSchema")
        return err.args[0], ""
    except requests.exceptions.ConnectionError as err:
        logger.warning("Error with request occured: ConnectionError")
        return "ConnectionError:<br>" \
               "* please check if " + passed_url + " exists<br>" \
                                                   "* please check your internet connection and try again", ""
    if r.request.path_url == '/':
        base_url = r.request.url[0:len(r.request.url) - 1]
    else:
        base_url = r.request.url.replace(r.request.path_url, "")

    if r.status_code == 404:
        logger.warning("Error with request occured: HTTP status: 404")
        return "Passed url not found. Error code: 404. Please check if " + passed_url + " exists", ""
    elif r.status_code == 400:
        logger.warning("Error with request occured: HTTP status: 400")
        return "Bad request. Error code: 400. Server " + base_url + " could not understand request: " + passed_url, ""
    elif r.status_code == 503:
        logger.warning("Error with request occured: HTTP status: 503")
        return "Service Unavailable. Error code: 503. Server " + base_url + "may be overloaded. Please try again " \
                                                                            "later. ", ""
    elif r.status_code > 399:
        logger.warning("Error with request occured: HTTP status: " + str(r.status_code))
        return "Something gone wrong. Error code: " + str(r.status_code), ""
    logger.info("Request has succeeded: " + passed_url)
    return r, base_url


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
