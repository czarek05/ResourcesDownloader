import unittest
from unittest.mock import patch, mock_open, Mock

import responses

import constants as cons
from app import app
import logging


class ResourceDownloaderTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    @responses.activate
    def test_not_found(self):
        responses.add(responses.GET, 'https://test1.pl',
                      status=404)

        with app.test_client() as c:
            rv = c.get('/img/https://test1.pl')
            self.assertTrue("Passed url not found. Error code: 404. Please check" in str(rv.data))

    @responses.activate
    def test_invalid_argument(self):
        responses.add(responses.GET, 'abcde')

        with app.test_client() as c:
            rv = c.get('/img/abcde')
            self.assertTrue("No schema supplied. Perhaps you meant" in str(rv.data))

    @responses.activate
    def test_bad_request(self):
        responses.add(responses.GET, 'https://test.pl/',
                      status=400)

        with app.test_client() as c:
            rv = c.get('/text/https://test.pl/')
            self.assertTrue("Bad request. Error code: 400. Server" in str(rv.data))

    @responses.activate
    def test_service_unavailable(self):
        responses.add(responses.GET, 'https://test2.pl/',
                      status=503)

        with app.test_client() as c:
            rv = c.get('/text/https://test2.pl/')
            self.assertTrue("Service Unavailable. Error code: 503." in str(rv.data))

    @responses.activate
    def test_general_error_code(self):
        responses.add(responses.GET, 'https://test3.pl/',
                      status=402)

        with app.test_client() as c:
            rv = c.get('/text/https://test3.pl/')
            self.assertTrue("Something gone wrong. Error code:" in str(rv.data))

    @responses.activate
    def test_text_happy_path(self):
        responses.add(responses.GET, 'https://test2.pl',
                      status=200,
                      body=cons.HTML_PAGE_1)

        with app.test_client() as c:
            open_mock = mock_open()
            with patch("builtins.open", open_mock, create=True):
                rv = c.get('/text/https://test2.pl')
                self.assertTrue(open_mock.called)
                open_mock.return_value.write.assert_called_with(cons.SCRAPED_TEXT_HTML_PAGE_1)

    @responses.activate
    def test_img_happy_path(self):
        responses.add(responses.GET, 'https://test3.pl',
                      status=200,
                      body=cons.HTML_PAGE_2)

        with app.test_client() as c:
            mock = Mock()
            with patch("urllib.request.urlretrieve", mock, create=True):
                rv = c.get('/img/https://test3.pl')
                self.assertTrue(mock.call_count == 6)
                self.assertTrue(mock.call_args_list[0][0][0] == cons.SCRAPED_IMG_1)
                self.assertTrue(mock.call_args_list[0][0][1].startswith("scraped_data/"))

                self.assertTrue(mock.call_args_list[1][0][0] == cons.SCRAPED_IMG_2)
                self.assertTrue(mock.call_args_list[1][0][1].startswith("scraped_data/"))

                self.assertTrue(mock.call_args_list[2][0][0] == cons.SCRAPED_IMG_3)
                self.assertTrue(mock.call_args_list[2][0][1].startswith("scraped_data/"))

                self.assertTrue(mock.call_args_list[3][0][0] == cons.SCRAPED_IMG_4)
                self.assertTrue(mock.call_args_list[3][0][1].startswith("scraped_data/"))

                self.assertTrue(mock.call_args_list[4][0][0] == cons.SCRAPED_IMG_5)
                self.assertTrue(mock.call_args_list[4][0][1].startswith("scraped_data/"))

                self.assertTrue(mock.call_args_list[5][0][0] == cons.SCRAPED_IMG_6)
                self.assertTrue(mock.call_args_list[5][0][1].startswith("scraped_data/"))


if __name__ == '__main__':
    unittest.main()
