from abc import ABC
from html.parser import HTMLParser


class TextHTMLParser(HTMLParser, ABC):

    def __init__(self):
        super().__init__()
        self.found_text = []

    def handle_starttag(self, tag, attrs):
        pass

    def handle_endtag(self, tag):
        pass

    def handle_data(self, data):
        # omitting inner HTML if contains only '\n' characters
        temp = data.replace("\n", "")
        if temp:
            self.found_text.append(data)
        pass
