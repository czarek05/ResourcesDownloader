FROM python:3

COPY . /app

WORKDIR /app

RUN pip install -r requirements.txt

ENTRYPOINT ["python"]

ARG DISABLE_CACHE=1

RUN python test.py

CMD ["app.py"]