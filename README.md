# ResourcesDownloader

Mikroserwis umożliwiający pobieranie statycznych obrazów i tekstu ze stron internetowych
oraz wysyłający pobrane zasoby do użytkownika. W projekcie wykorzystano między innymi:
Flask, Beautiful Soup, Docker.


## Komentarz autora
Zrealizowano funkcjonalności pobierania tekstu, obrazów ze strony internetowych, wysyłania zasobów
do użytkownika. W funkcji wykonującej żądanie do zewnętrznego serwisu w celu pobrania kodu HTML
dodano obsługę kodów błędu odpowiedzi HTTP: użytkownikowi wysyłana jest informacja nt. tego, co mogło
pójśc nie tak, ewentualnie informacja o błedzie i kod błędu. Aplikacja jest w stanie pobrać statyczne
obrazy umieszczone z wykorzystaniem następujących znaczników, atrybutów HTML:
1. img src="..."  
2. img data-src="..." 
3. img data-original="..." 
4. meta content="..." 
5. link href="..." 
6. div data-background="..."

Do rozważenia w przyszłości jest pomysł poszerzenie tej listy o pozostałe znaczniki, które można by
obsłużyć. Np.:

section data-pagespeed-no-defer style="background-image: url('https://test.pl/images/328.jpg'); background-position:center center;" class="cover page-cover" id="top_bar_picture"

Niepowodzenia, które mogą pojawić się podczas korzystania funkcjonalności pobierania obrazów mogą być spowodowane np.:

1. Błędem po stronie zewnętrznego serwisu (url do nieistniejącego zasobu).
2. Zabezpieczeniem antywebscrapingowe.
3. Wystąpieniem URL, który oszuka zastosowany w aplikacji algorytm. Przykład z https://youtube.com:

img src="https://i.ytimg.com/vi/W2EZoXedIQE/hq720.jpg?sqp=-oaymwEZCOgCEMoBSFXyq4qpAwsIARUAAIhCGAFwAQ==&amp;rs=AOn4CLDJ4GLUtWQH5Ur5Ydbysm6wQoMWng"

Mimo rozszerzenia "jpg" w nazwie, tak naprawdę powyższy obraz ma rozszerzenie "webp". Pewnym rozwiązaniem
tego problemu mogłoby być zaimplementowanie funkcjonalności, która sprawdzałaby poprawność rozszerzenia obrazu
i w razie nieprawidłowości - przeprowadzała zmianę rozszerzenia na .webp. W razie dalszych nieprawidłowości
proces ten byłby powtarzany z innym rozszerzeniem.


Ciekawą kwestią zaobserwowaną w trakcie realizacji projektu jest fakt, że dodanie odpowiednich nagłówków
do żądania pozwala otrzymać prawidłową odpowiedź od zewnętrznych serwisów. Np.:
1. "user-content" - https://transfermarkt.com
2. "Accept" - https://wp.pl

Bez tych nagłówków, powyższe serwisy nie odesłałyby pozytywnej odpowiedzi, a pobieranie zasobów nie 
powiodłoby się. Należy jednak pamiętać, że zbyt częste próby pobrania zasobów z niektórych
serwisów mogłyby skończyć się blokadą.

Być może wartym rozważenia byłoby umożliwienie pobierania zgromadzonych zasobów także w inny sposób
(obecnie tworzone jest archiwum zawierające wszystkie dotychczas pobrane zasoby i to archiwum
jest wysyłane do użytkownika).

Nie udało się zaimplementować funkcjonalności sprawdzania statusu zleconego zadania. Pewnym rozwiązaniem
mogłoby być zapisywanie informacji o zakończonych zleceniach do pliku oraz rozszerzenie API o funkcjonalność
wysłania treści tego pliku użytkownikowi. Nie pozwoliłoby to jednak informować o statusie zleceń, które
są w trakcie realizacji. Należałoby się zastanowić, jak to wykonać. 

## API

GET /text/{URL}

Zlecenie pobrania tekstu z podanego URL. Przykładowe użycie:

/text/https://stackoverflow.com

GET /img/{URL}

Zlecenie pobrania statycznych obrazów umieszczonych pod podanym URL. Przykładowe użycie:

/img/https://stackoverflow.com

GET /download/{URL}

Pobranie zgromadzonych przez mikroserwis zasobów. Przykładowe użycie:

/download/


## Instrukcja uruchamiania z użyciem Dockera 
1. Sklonować projekt
2. Uruchomić terminal w lokalizacji, gdzie został ściągnięty plik Dockerfile
3. W terminalu użyć poniższej komendy, której zadaniem jest przeprowadzenie testów i stworzenie kontenera.
4. docker build -t resources_downloader --build-arg DISABLE_CACHE=$(date +%s) .
5. Sprawdzić id {image id} stworzonego obrazu (komenda: docker images) i za pomocą poniższej komendy uruchomić aplikację:
6. docker run -d -p 5000:5000 {image id}

Aplikacja będzie dostępna za pośrednictwem ip, pod którym działa Docker, pod portem 5000. Przykładowe wywołanie:

http://192.168.99.100:5000/text/https://stackoverflow.com